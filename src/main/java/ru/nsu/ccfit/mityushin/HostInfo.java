package ru.nsu.ccfit.mityushin;

import java.net.InetAddress;

public class HostInfo {
    private InetAddress ipAddress;
    private int port;

    public HostInfo(InetAddress ipAddress, int port) {
        this.ipAddress = ipAddress;
        this.port = port;
    }

    @Override
    public int hashCode() {
        return ipAddress.hashCode() + port;
    }

    @Override
    public boolean equals(Object obj) {
        return ((this.ipAddress.equals(((HostInfo) obj).ipAddress)) && (port == ((HostInfo) obj).port));
    }

    public int getPort() {
        return port;
    }

    public InetAddress getIpAddress() {
        return ipAddress;
    }

    public boolean equalsTo(HostInfo h) {
        return ((this.ipAddress.equals(h.ipAddress)) && (this.port == h.port));
    }

}
