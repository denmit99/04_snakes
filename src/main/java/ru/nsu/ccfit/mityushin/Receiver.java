package ru.nsu.ccfit.mityushin;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

public class Receiver implements Runnable {

    private DatagramSocket socket;
    private Game game;
    private MessageNumerator messageNumerator;
    private HashMap<Long, Message> unconfirmedMessages;
    private Sender sender;
    private HashMap<HostInfo, AtomicLong> lastMessageTime;
    private final int BUFSIZE = 1024;

    public Receiver(DatagramSocket socket, Game game, MessageNumerator messageNumerator, Sender sender, HashMap<HostInfo, AtomicLong> lastMessageTime) {
        this.socket = socket;
        this.game = game;
        this.messageNumerator = messageNumerator;
        this.unconfirmedMessages = sender.getUnconfirmedMessages();
        this.sender = sender;
        this.lastMessageTime = lastMessageTime;
    }

    public void run() {
        try {
            while (true) {

                DatagramPacket receivedPacket = new DatagramPacket(new byte[BUFSIZE], BUFSIZE);
                socket.receive(receivedPacket);
                byte[] msg = Arrays.copyOfRange(receivedPacket.getData(), 0, receivedPacket.getLength());
                SnakesProto.GameMessage gameMessage = SnakesProto.GameMessage.parseFrom(msg);

                updateLastActivity(receivedPacket.getAddress(), receivedPacket.getPort());

                switch (gameMessage.getTypeCase()) {
                    case JOIN:
                        SnakesProto.GameMessage.JoinMsg joinMessage = gameMessage.getJoin();
                        String joinerIp = receivedPacket.getAddress().getHostAddress();
                        int joinerPort = receivedPacket.getPort();
                        System.out.println(joinerIp + ":" + joinerPort + " (" + joinMessage.getName() + ") wants to join");

                        if (!game.isPlayer(joinerIp, joinerPort)) { //если пришел JOIN от игрока который уже добавлен, игнорим

                            Player newPlayer = new Player(joinMessage.getName(), SnakesProto.NodeRole.NORMAL, 0, joinerIp, joinerPort);

                            boolean isAdded = game.addNewPlayer(newPlayer);
                            int newPlayerId = newPlayer.getId();
                            System.out.println("new player id is " + newPlayerId);

                            if (isAdded) {
                                System.out.println("Player " + joinMessage.getName() + " added");
                                SnakesProto.GameMessage ackMsg = MessageCreator.createAckMsg(gameMessage.getMsgSeq(), game.getMainPlayer().getId(), newPlayerId);
                                System.out.println("ack for join sender:" + game.getMainPlayer().getId() + " receiver: " + newPlayerId);
                                sender.send(ackMsg, receivedPacket.getAddress(), joinerPort, true);
                                if (game.getPlayers().size() == 2) {
                                    newPlayer.setRole(SnakesProto.NodeRole.DEPUTY);
                                    SnakesProto.GameMessage roleChangeMsg = MessageCreator.createRoleChangeMsg(messageNumerator.getNewMessageNumber(), game.getMainPlayer().getId(), newPlayerId, SnakesProto.NodeRole.MASTER, SnakesProto.NodeRole.DEPUTY);
                                    System.out.println("I WANT NEW PLAYER TO BE " + roleChangeMsg.getRoleChange().getReceiverRole().toString());
                                    sender.send(roleChangeMsg, receivedPacket.getAddress(), joinerPort, true);

                                }

                            } else {
                                System.out.println("Error while adding new player to the game");
                                SnakesProto.GameMessage errorMsg = MessageCreator.createErrorMsg(messageNumerator.getNewMessageNumber(), "Error while adding new player to the game");
                                sender.send(errorMsg, receivedPacket.getAddress(), joinerPort, true);
                            }
                        } else System.out.println("This player is already in the game. Join message was ignored");
                        break;
                    case STEER:
                        SnakesProto.GameMessage.SteerMsg steerMessage = gameMessage.getSteer();
                        SnakesProto.Direction direction = steerMessage.getDirection();
                        System.out.println("steer received from " + receivedPacket.getAddress() + ":" + receivedPacket.getPort());

                        SnakesProto.GameMessage ackMsg = MessageCreator.createAckMsg(gameMessage.getMsgSeq(), game.getMainPlayer().getId(), gameMessage.getSenderId());

                        sender.send(ackMsg, receivedPacket.getAddress(), receivedPacket.getPort(), true);
                        System.out.println("ACK CREATED, sender: " + game.getMainPlayer().getId() + ", receiver: " + gameMessage.getSenderId());

                        for (Player p : game.getPlayers()) {
                            if (p.getIpAddress().equals(receivedPacket.getAddress().getHostAddress()) && (p.getPort() == receivedPacket.getPort())) {
                                System.out.println("Steer received. Change direction");
                                p.getSnake().changeDirection(direction);
                            }
                        }
                        break;

                    case ACK:
                        synchronized (unconfirmedMessages) {
                            unconfirmedMessages.remove(gameMessage.getMsgSeq());
                        }
                        System.out.println("Message " + gameMessage.getMsgSeq() + " confirmed.");
                        break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateLastActivity(InetAddress ip, int port) {
        for (Player p : game.getPlayers()) {
            if ((p.getIpAddress().equals(ip.getHostAddress())) && (p.getPort() == port)) {
                //System.out.println("last activity changed");
                p.setLastActivity(System.currentTimeMillis());
            }
        }
    }
}
