package ru.nsu.ccfit.mityushin;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class GamePanel extends JFrame {
    //private GameField gameField;
    private JLabel numOfFood;
    private JLabel myScore;
    private Player player;
    private JButton[][] cells;
    private MessageNumerator messageNumerator;
    private final DatagramSocket socket;
    private InetAddress masterIP;
    private int masterPort;
    private static final int CELL_DIMENSION = 25;
    private Sender sender;

    public GamePanel(final GameField gameField, final MessageNumerator messageNumerator, final DatagramSocket socket) {
        super("Snake");
        //this.gameField = gameField;
        cells = new JButton[gameField.getHeight()][gameField.getWidth()];
        this.messageNumerator = messageNumerator;
        this.socket = socket;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(gameField.getWidth() * CELL_DIMENSION + 20, gameField.getHeight() * CELL_DIMENSION + 100);
        setVisible(true);
        JPanel mainPanel = new JPanel();
        mainPanel.add(new JLabel("Food: "));

        addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                //System.out.println("key pressed");
                if ((player != null) && ((e.getKeyCode() == KeyEvent.VK_UP) || (e.getKeyCode() == KeyEvent.VK_DOWN) || (e.getKeyCode() == KeyEvent.VK_LEFT) || (e.getKeyCode() == KeyEvent.VK_RIGHT))) {
                    if (player.getRole() == SnakesProto.NodeRole.MASTER) {
                        switch (e.getKeyCode()) {
                            case KeyEvent.VK_UP:
                                player.getSnake().changeDirection(SnakesProto.Direction.UP);
                                break;
                            case KeyEvent.VK_LEFT:
                                player.getSnake().changeDirection(SnakesProto.Direction.LEFT);
                                break;
                            case KeyEvent.VK_DOWN:
                                player.getSnake().changeDirection(SnakesProto.Direction.DOWN);
                                break;
                            case KeyEvent.VK_RIGHT:
                                player.getSnake().changeDirection(SnakesProto.Direction.RIGHT);
                                break;
                        }
                    } else {
                        if (player.getSnake().getSnakeState() != SnakesProto.GameState.Snake.SnakeState.ZOMBIE) {
                            System.out.println("Steer from " + player.getRole());
                            switch (e.getKeyCode()) {
                                case KeyEvent.VK_UP:
                                    sender.send(MessageCreator.createSteerMsg(messageNumerator.getNewMessageNumber(), player.getId(), SnakesProto.Direction.UP), masterIP, masterPort, true);
                                    break;
                                case KeyEvent.VK_LEFT:
                                    sender.send(MessageCreator.createSteerMsg(messageNumerator.getNewMessageNumber(), player.getId(), SnakesProto.Direction.LEFT), masterIP, masterPort, true);
                                    break;
                                case KeyEvent.VK_DOWN:
                                    sender.send(MessageCreator.createSteerMsg(messageNumerator.getNewMessageNumber(), player.getId(), SnakesProto.Direction.DOWN), masterIP, masterPort, true);
                                    System.out.println(player.getId());
                                    break;
                                case KeyEvent.VK_RIGHT:
                                    sender.send(MessageCreator.createSteerMsg(messageNumerator.getNewMessageNumber(), player.getId(), SnakesProto.Direction.RIGHT), masterIP, masterPort, true);
                                    System.out.println(player.getId());
                                    break;
                            }

                            System.out.println("steer sended");
                        }
                    }
                }
            }
        });

        numOfFood = new JLabel("0");
        mainPanel.add(numOfFood);
        mainPanel.add(new JLabel("Score: "));
        myScore = new JLabel("0");
        mainPanel.add(myScore);
        JPanel fieldPanel = new JPanel();
        fieldPanel.setLayout(new GridLayout(gameField.getHeight(), gameField.getWidth(), 0, 0));
        fieldPanel.setSize(gameField.getWidth(), gameField.getHeight());
        for (int i = 0; i < gameField.getHeight(); i++) {
            for (int j = 0; j < gameField.getWidth(); j++) {
                JButton button = new JButton();
                button.setPreferredSize(new Dimension(CELL_DIMENSION, CELL_DIMENSION));
                button.setBackground(Color.black);
                button.setEnabled(false);
                cells[i][j] = button;
                fieldPanel.add(button);
            }
        }
        mainPanel.add(fieldPanel);
        setContentPane(mainPanel);
    }

    public JButton[][] getCells() {
        return cells;
    }

    public JLabel getNumOfFood() {
        return numOfFood;
    }

    public JLabel getScore() {
        return myScore;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public InetAddress getMasterIP() {
        return masterIP;
    }

    public void setMasterIP(InetAddress masterIP) {
        this.masterIP = masterIP;
    }

    public int getMasterPort() {
        return masterPort;
    }

    public void setMasterPort(int masterPort) {
        this.masterPort = masterPort;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }
}
