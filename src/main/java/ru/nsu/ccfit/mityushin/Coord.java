package ru.nsu.ccfit.mityushin;

public class Coord {
    private int x;
    private int y;

    public Coord(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Coord(Coord c) {
        this.x = c.x;
        this.y = c.y;
    }

    public Coord(SnakesProto.GameState.Coord c) {
        x = c.getX();
        y = c.getY();
    }

    public int getX() {
        return x;
    }

    public boolean equals(Coord c) {
        return ((this.x == c.x) && (this.y == c.y));
    }

    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Coord))
            return false;
        Coord c = (Coord) o;
        return ((this.x == c.x) && (this.y == c.y));
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    public Coord minus(Coord c) {
        return new Coord(x - c.getX(), y - c.getY());
    }

    public Coord plus(Coord c) {
        return new Coord(x + c.getX(), y + c.getY());
    }


}
