package ru.nsu.ccfit.mityushin;

public class Cell {
    private Coord coord;
    private CellType cellType = CellType.EMPTY;

    public Cell(Coord c) {
        coord = c;
    }

    public Cell(int x, int y) {
        coord = new Coord(x, y);
    }

    public CellType getCellType() {
        return cellType;
    }

    public void setCellType(CellType cellType) {
        this.cellType = cellType;
    }

    public Coord getCoord() {
        return coord;
    }
}
