package ru.nsu.ccfit.mityushin;

public class MessageNumerator {
    private int messageNumber = 0;

    public synchronized int getNewMessageNumber() {
        int lastMessageNumber = messageNumber;
        messageNumber++;
        return lastMessageNumber;
    }
}
