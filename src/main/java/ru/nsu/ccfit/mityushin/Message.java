package ru.nsu.ccfit.mityushin;

import java.net.InetAddress;

public class Message {
    private SnakesProto.GameMessage gameMessage;
    private InetAddress receiverIP;
    private int receiverPort;

    public Message(SnakesProto.GameMessage gameMessage, InetAddress receiverIP,  int receiverPort){
        this.gameMessage = gameMessage;
        this.receiverIP = receiverIP;
        this.receiverPort = receiverPort;
    }

    public InetAddress getReceiverIP() {
        return receiverIP;
    }

    public void setReceiverIP(InetAddress receiverIP) {
        this.receiverIP = receiverIP;
    }

    public SnakesProto.GameMessage getGameMessage() {
        return gameMessage;
    }

    public void setGameMessage(SnakesProto.GameMessage gameMessage) {
        this.gameMessage = gameMessage;
    }

    public int getReceiverPort() {
        return receiverPort;
    }

    public void setReceiverPort(int receiverPort) {
        this.receiverPort = receiverPort;
    }
}
