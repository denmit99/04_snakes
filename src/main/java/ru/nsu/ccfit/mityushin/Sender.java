package ru.nsu.ccfit.mityushin;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

public class Sender {

    private final DatagramSocket socket;
    private final HashMap<Long, Message> unconfirmedMessages;
    private final HashMap<Long, Long> confirmationTable;
    private final HashMap<HostInfo, AtomicLong> lastMessageTime; //время отправки последнего сообщения узлу


    public Sender(DatagramSocket socket, HashMap<HostInfo, AtomicLong> lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
        unconfirmedMessages = new HashMap<>();
        confirmationTable = new HashMap<>();
        this.socket = socket;
    }

    public void send(SnakesProto.GameMessage message, InetAddress receiverIP, int receiverPort, boolean isNew) {

        HostInfo hostInfo = new HostInfo(receiverIP, receiverPort);
        try {
                synchronized (lastMessageTime) {
                    if (lastMessageTime.containsKey(hostInfo)) {
                        lastMessageTime.get(hostInfo).set(System.currentTimeMillis());
                        System.out.println("info in lastmessage updated");
                        //System.out.println("last time message for " + message.getReceiverId() + " changed");
                    } else {
                        lastMessageTime.put(hostInfo, new AtomicLong(System.currentTimeMillis()));
                        System.out.println("new " + message.getReceiverId() + " added to lastMessageTime. Message [" + message.getTypeCase().toString() + "]");
                    }
                }
                System.out.println("before switch");
                switch (message.getTypeCase()) {
                    case ACK:
                        break;
                    case ANNOUNCEMENT:
                        break;
                    case ERROR:
                    case ROLE_CHANGE:
                    case STATE:
                    case STEER:
                    case JOIN:
                        if (isNew) {
                            //lastMessageTime.set(System.currentTimeMillis());
                            synchronized (unconfirmedMessages) {

                                System.out.println("lock (unconfirmedMessages) in sender");
                                System.out.println("before putting to unconfirmed");
                                unconfirmedMessages.put(message.getMsgSeq(), new Message(message, receiverIP, receiverPort));
                                System.out.println("................................." + message.getMsgSeq() + " added to unconfirmed");
                            }

                            System.out.println("unlock (unconfirmedMessages) in sender");
                        } else {
                            /*
                            * Не пытаться переслать соообщени игроку который уже отключился
                            * */
                            System.out.println("its resending");
                        }
                        synchronized (confirmationTable) {
                            System.out.println("lock (confirmationTable) in sender");
                            confirmationTable.put(message.getMsgSeq(), System.currentTimeMillis());
                            System.out.println("................................." + message.getMsgSeq() + " added/update in confirmation table");
                        }
                        System.out.println("unlock (confirmationTable) in sender");
                        break;
                }
                System.out.println("before send in sender");
                socket.send(new DatagramPacket(message.toByteArray(), message.toByteArray().length, receiverIP, receiverPort));
                System.out.println("after send in sender");
                //if (!message.getTypeCase().toString().equals("PING"))
                System.out.println("Message " + message.getMsgSeq() + " send [" + message.getTypeCase().toString() + "] to " + receiverIP + ":" + receiverPort);

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error while sending message");
        }
    }

    public HashMap<Long, Message> getUnconfirmedMessages() {
        return unconfirmedMessages;
    }

    public HashMap<Long, Long> getConfirmationTable() {
        return confirmationTable;
    }

    public HashMap<HostInfo, AtomicLong> getLastMessageTime() {
        return lastMessageTime;
    }
}
