package ru.nsu.ccfit.mityushin;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class PingSender implements Runnable {

    private final DatagramSocket socket;
    private final Sender sender;
    private final HashMap<HostInfo, AtomicLong> lastMessageTime;
    private MessageNumerator messageNumerator;
    private ArrayList<Player> players;
    private int pingDelay;

    public PingSender(DatagramSocket socket, Sender sender, MessageNumerator messageNumerator, ArrayList<Player> players, int pingDelay) {
        this.socket = socket;
        this.sender = sender;
        lastMessageTime = sender.getLastMessageTime();
        this.messageNumerator = messageNumerator;
        this.players = players;
        this.pingDelay = pingDelay;
    }

    public void run() {
        while (true) {
            synchronized (lastMessageTime) {
                Iterator it = lastMessageTime.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    HostInfo hostInfo = (HostInfo) pair.getKey();
                    AtomicLong lastMsg = (AtomicLong) pair.getValue();
                    InetAddress playerIP = hostInfo.getIpAddress();
                    int playerPort = hostInfo.getPort();
                    //System.out.println(playerID + " last msg - " + lastMsg.get());
                    long delay = System.currentTimeMillis() - lastMsg.get();
                    if (delay > pingDelay) {
                        //Player receiver = getPlayerByID(players, playerID);
                        Player receiver = Player.getPlayerByIpAndPort(players, playerIP, playerPort);
                        if (receiver != null) {
                            SnakesProto.GameMessage pingMsg = MessageCreator.createPingMsg(messageNumerator.getNewMessageNumber());
                            //System.out.println(delay + ">" + pingDelay);
                            sender.send(pingMsg, playerIP, playerPort, true);
                            //System.out.println("Ping sended to " + receiver.getIpAddress() + ":" + receiver.getPort() + " after delay " + delay);
                        }
                    }
                }
            }
        }
    }

    public Player getPlayerByID(ArrayList<Player> players, int id) {
        for (Player p : players) {
            if (p.getId() == id) {
                return p;
            }
        }
        return null;
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }
}
