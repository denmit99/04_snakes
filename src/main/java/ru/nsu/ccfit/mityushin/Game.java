package ru.nsu.ccfit.mityushin;

import com.google.protobuf.InvalidProtocolBufferException;

import java.awt.*;
import java.io.IOException;
import java.net.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static java.lang.Math.abs;
import static ru.nsu.ccfit.mityushin.SnakesProto.NodeRole.*;

public class Game {
    private Player mainPlayer;
    private Player masterPlayer;
    private ArrayList<Player> players = new ArrayList<Player>();
    private ArrayList<Coord> food = new ArrayList<Coord>();
    private GameConfig gameConfig;
    private GameField gameField;
    private GamePanel gamePanel;
    private DatagramSocket socket;
    private MessageNumerator messageNumerator;
    private HashMap<Long, Message> unconfirmedMessages;
    private Sender sender;
    private HashMap<HostInfo, AtomicLong> lastMessageTime;
    private int newPlayerId = 0;
    private int stateOrder = 0;
    private static final int BUFSIZE = 1024;

    public Game(GameConfig gameConfig, DatagramSocket socket, MessageNumerator messageNumerator, Sender sender, HashMap<HostInfo, AtomicLong> lastMessageTime) {
        this.gameConfig = gameConfig;
        this.socket = socket;
        this.messageNumerator = messageNumerator;
        this.unconfirmedMessages = sender.getUnconfirmedMessages();
        this.sender = sender;
        this.lastMessageTime = lastMessageTime;
        if (gameConfig != null) {
            gameField = new GameField(gameConfig.getWidth(), gameConfig.getHeight());
            gamePanel = new GamePanel(gameField, messageNumerator, socket);
        } else System.out.println("gameConfig is  null. Impossible to create a game");
    }

    public Game(GameConfig gameConfig, DatagramSocket socket, MessageNumerator messageNumerator, Sender sender, HashMap<HostInfo, AtomicLong> lastMessageTime, Player player) {
        this.gameConfig = gameConfig;
        this.messageNumerator = messageNumerator;
        this.unconfirmedMessages = sender.getUnconfirmedMessages();
        this.sender = sender;
        this.lastMessageTime = lastMessageTime;
        mainPlayer = player;
        this.socket = socket;
        if (gameConfig != null) {
            gameField = new GameField(gameConfig.getWidth(), gameConfig.getHeight());
            gamePanel = new GamePanel(gameField, messageNumerator, socket);
        } else System.out.println("gameConfig is  null. Impossible to create a game");
    }

    public void start(InetAddress ip, int port) {
        Thread resender = new Thread(new Resender(socket, sender, players, gameConfig.getPingDelayMs()));
        resender.start();
        gamePanel.setTitle(mainPlayer.getName());
        gamePanel.setPlayer(mainPlayer);
        long prevTime = 0, curTime;
        int lastStateOrder = -1;
        Thread pingSender = new Thread(new PingSender(socket, sender, messageNumerator, players, gameConfig.getPingDelayMs()));
        pingSender.start();
        if (mainPlayer.getRole() == MASTER) {
            masterPlayer = mainPlayer;
            Thread announcementSender = new Thread(new AnnouncementSender(9192, "239.192.0.4", socket, gameConfig, players, messageNumerator));
            announcementSender.start();
            placeFood();

            /*Thread pingSender = new Thread(new PingSender(socket, sender, messageNumerator, players, gameConfig.getPingDelayMs()));
            pingSender.start();*/
        } else if (mainPlayer.getRole() == NORMAL) {
            gamePanel.setMasterIP(ip);
            gamePanel.setMasterPort(port);
            gamePanel.setSender(sender);
            mainPlayer.setSnake(new Snake());

            masterPlayer = new Player("", MASTER, -1, ip.getHostAddress(), port);
            players.add(masterPlayer);

            /*Thread pingSender = new Thread(new PingSender(socket, sender, messageNumerator, players, gameConfig.getPingDelayMs()));
            pingSender.start();*/
            try {
                socket.setSoTimeout(gameConfig.getNodeTimeoutMs());
            } catch (SocketException e) {
                e.printStackTrace();
            }
        }
        while (true) {
            switch (mainPlayer.getRole()) {
                case MASTER:
                    curTime = System.currentTimeMillis();
                    if (curTime - prevTime > gameConfig.getStateDelayMs()) {

                        System.out.println("\n************");
                        System.out.println("_________________" + mainPlayer.getRole() + "__________________");
                        for (Player p : players) {
                            if (!(p.getRole() == SnakesProto.NodeRole.VIEWER)) {
                                Coord nextHeadCoords = convertCoord(p.getSnake().getNextCoord());
                                if (gameField.get(nextHeadCoords).getCellType() == CellType.FOOD) {
                                    eatFood(nextHeadCoords); //съесть еду
                                    p.getSnake().growUp(nextHeadCoords); //вырасти
                                    p.addPointsToScore(1); //добавить очки
                                } else {

                                    p.getSnake().moveForward(nextHeadCoords);
                                }
                            }
                        }

                        checkDeaths();
                        System.out.println("check deaths");

                        if (food.size() < getNumberOfFood()) {
                            placeFood();
                        }
                        refreshSnakes();
                        System.out.println("refresh snakes");
                        sendStateToPlayers();
                        System.out.println("send state to players");
                        drawGraphic();
                        System.out.println("graphic drawn");
                        prevTime = curTime;

                        System.out.println("NUMBER OF PLAYERS: " + players.size());
                    }
                    //System.out.println("check players activity");
                    checkPlayersActivity();
                    break;
                case DEPUTY:
                case VIEWER:
                case NORMAL:
                    try {
                        DatagramPacket receivedPacket = new DatagramPacket(new byte[BUFSIZE], BUFSIZE);
                        socket.receive(receivedPacket);
                        System.out.println("************");
                        byte[] msg = Arrays.copyOfRange(receivedPacket.getData(), 0, receivedPacket.getLength());
                        SnakesProto.GameMessage gameMessage = SnakesProto.GameMessage.parseFrom(msg);
                        System.out.println("Message " + gameMessage.getMsgSeq() + " [" + gameMessage.getTypeCase().toString() + "] received");
                        SnakesProto.GameMessage ackMsg = MessageCreator.createAckMsg(gameMessage.getMsgSeq(), mainPlayer.getId());
                        switch (gameMessage.getTypeCase()) {
                            case STATE:
                                sender.send(ackMsg, receivedPacket.getAddress(), receivedPacket.getPort(), true);
                                int stateOrder = gameMessage.getState().getState().getStateOrder();
                                if (stateOrder > lastStateOrder) {
                                    SnakesProto.GameMessage.StateMsg stateMessage = gameMessage.getState();
                                    System.out.println("State " + stateOrder + " received");
                                    setGameState(stateMessage.getState(), mainPlayer.getRole());
                                    lastStateOrder = stateOrder;
                                    drawGraphic();
                                }
                                break;
                            case ACK:
                                if (masterPlayer.getId() == -1) { //ЭТО НИКОГДА НЕ ДЕЛАЕТСЯ, НАДО СДЕЛАТЬ ЧТОБЫ ИГРА У ДЖОЙНЕРА НЕ НАЧИНАЛАСЬ ПОКА К НЕМУ НЕ ПРИДЕТ АСК с его ID тогда присовить его к mainPlayerId ну и все остальное, что в этом блоке
                                    mainPlayer.setId(gameMessage.getReceiverId());
                                    masterPlayer.setId(gameMessage.getSenderId());
                                }

                                synchronized (unconfirmedMessages) {
                                    unconfirmedMessages.remove(gameMessage.getMsgSeq());
                                }
                                System.out.println("Message " + gameMessage.getMsgSeq() + " confirmed.");
                                break;
                            case ROLE_CHANGE: {
                                sender.send(ackMsg, receivedPacket.getAddress(), receivedPacket.getPort(), true);
                                SnakesProto.NodeRole receiverRole = gameMessage.getRoleChange().getReceiverRole();
                                SnakesProto.NodeRole senderRole = gameMessage.getRoleChange().getSenderRole();
                                System.out.println(senderRole.toString() + " WANTS ME TO CHANGE ROLE TO: " + receiverRole.toString());
                                if ((senderRole == MASTER) && (receiverRole == DEPUTY)) {
                                    System.out.println(".....................................I am deputy!");
                                    mainPlayer.setRole(DEPUTY);
                                    //обозначить где-то у себя что мы это DEPUTY
                                } else if ((senderRole == MASTER) && (receiverRole == NORMAL)) {
                                    System.out.println("It's new master!");
                                    gamePanel.setMasterIP(receivedPacket.getAddress());
                                    gamePanel.setMasterPort(receivedPacket.getPort());
                                    masterPlayer = new Player("", MASTER, -1, receivedPacket.getAddress().getHostAddress(), receivedPacket.getPort());
                                    players.add(masterPlayer);
                                    lastStateOrder = -1;
                                } else if ((senderRole == MASTER) && (receiverRole == VIEWER)) {
                                    System.out.println(".......................................I am dead!");
                                    mainPlayer.setRole(VIEWER);
                                }
                            }
                            default:
                                break;
                        }
                    } catch (SocketTimeoutException e) {
                        System.out.println("It seems like MASTER left the game!");
                        if (mainPlayer.getRole() == DEPUTY) {
                            System.out.println("Try to delete player with id " + masterPlayer.getId() + "from players");
                            Player p = getPlayerById(masterPlayer.getId(), players);
                            if (p != null)
                                players.remove(p);
                            mainPlayer = getPlayerById(mainPlayer.getId(), players); //получить ссылку на самого себя из списка игроков
                            masterPlayer = mainPlayer;
                            mainPlayer.setRole(MASTER);
                            gamePanel.setPlayer(mainPlayer);
                            try {
                                socket.setSoTimeout(0);
                            } catch (SocketException e1) {
                                e1.printStackTrace();
                            }

                            Thread receiver = new Thread(new Receiver(socket, this, messageNumerator, sender, lastMessageTime));
                            receiver.start();
                            Thread announcementSender = new Thread(new AnnouncementSender(9192, "239.192.0.4", socket, gameConfig, players, messageNumerator));
                            announcementSender.start();
                            notifyAboutNewMaster();
                            chooseNewDeputy();
                            System.out.println("Master left the game. Now I am master");

                        }
                    } catch (InvalidProtocolBufferException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;

            }
        }
    }

    public Coord findCellForPlayer() { //возвращает клетку на поле, в которой можно появиться, если такой нет, то null
        int maxX = gameField.getHeight() - 1;
        int maxY = gameField.getWidth() - 1;
        Cell[][] cells = gameField.getCells();

        for (int i = 0; i < maxX; i++) {
            for (int j = 0; j < maxY; j++) {
                int count = 0;
                for (int k = 0; k <= 4; k++) {
                    for (int l = 0; l <= 4; l++) {
                        int x = (i + k > maxX) ? (i + k - 5) : (i + k);
                        int y = (j + l > maxY) ? (j + l - 5) : (j + l);
                        if ((x == maxX + 1) || (y == maxY + 1))
                            System.out.println("FUCK"); //по идее такого не должно быть
                        if (cells[x][y].getCellType() != CellType.EMPTY) {
                            count++;
                        }
                    }
                }
                if (count == 0) {
                    //System.out.println("cell (" + i + 2 + "," + j + 2 + ") is free");
                    return new Coord(i + 2, j + 2);
                }
            }
        }
        return null;
    }

    public Coord findRandomFreeCell() {
        Random random = new Random();
        Coord coord = null;

        if (gameField.getNumberOfEmptyCells() > 0) { //если есть место для еды
            while (true) {
                int randomX = abs(random.nextInt()) % gameConfig.getHeight();
                int randomY = abs(random.nextInt()) % gameConfig.getWidth();
                if (gameField.getCells()[randomX][randomY].getCellType() == CellType.EMPTY) {
                    coord = new Coord(randomX, randomY);
                    return coord;
                }
            }
        } else return null;
    }

    public boolean newPlayer(String name, SnakesProto.NodeRole nodeRole, int id, String ip, int port) {
        Coord c = findCellForPlayer();

        if (c == null) {
            System.out.println("No free cells!");
            return false;
        } else {
            Player player = new Player(c.getX(), c.getY(), name, nodeRole, id, ip, port);
            players.add(player);
            setPlayerPosition(player);
            return true;
        }
    }

    public boolean addNewPlayer(Player p) {
        Coord c = findCellForPlayer();

        if (c == null) {
            System.out.println("No free cells!");
            return false;
        } else {
            p.setSnake(new Snake(c.getX(), c.getY()));
            p.setId(getNewPlayerId());
            setPlayerPosition(p);
            synchronized (players) {
                players.add(p);
            }

            return true;
        }
    }

    public void refreshSnakes() {
        deleteSnakes();

        for (Player p : players) {
            if (p.getRole() != SnakesProto.NodeRole.VIEWER) {
                setPlayerPosition(p);
            }
        }
    }

    public void setPlayerPosition(Player p) { //установить состояние клеток поля в зависимости от координат змейки игрока
        ArrayList<Coord> points = p.getSnake().getPoints();

        for (Coord c : points) {
            int x = c.getX();
            int y = c.getY();
            gameField.getCells()[x][y].setCellType(CellType.SNAKE);
        }
    }

    public void deleteSnakes() {
        for (int i = 0; i < gameField.getHeight(); i++) {
            for (int j = 0; j < gameField.getWidth(); j++) {
                Cell c = gameField.getCells()[i][j];
                if (c.getCellType() == CellType.SNAKE)
                    c.setCellType(CellType.EMPTY);
            }
        }
    }

    public int getNumberOfFood() {
        return gameConfig.getFoodStatic() + (int) (gameConfig.getFoodPerPlayer() * getNumberOfAliveSnakes());
    }

    public void placeFood() { //разместить на поле столько еды сколько надо
        int numberOfFood = getNumberOfFood() - food.size();
        for (int i = 0; i < numberOfFood; i++) {
            Coord coord;
            if ((coord = findRandomFreeCell()) != null) {
                food.add(coord);
                gameField.getCells()[coord.getX()][coord.getY()].setCellType(CellType.FOOD);
            }
        }
    }

    public void drawField() {
        System.out.println("[Food: " + food.size() + "| Players: " + players.size() + "]");
        gameField.draw();
        System.out.println(" ");
    }

    public void drawGraphic() {
        for (int i = 0; i < gameField.getHeight(); i++) {
            for (int j = 0; j < gameField.getWidth(); j++) {
                switch (gameField.getCells()[i][j].getCellType()) {
                    case EMPTY:
                        gamePanel.getCells()[i][j].setBackground(Color.black);
                        break;
                    case FOOD:
                        gamePanel.getCells()[i][j].setBackground(Color.red);
                        break;
                    case SNAKE:
                        gamePanel.getCells()[i][j].setBackground(Color.green);
                        break;
                }
            }
        }
        gamePanel.getNumOfFood().setText(String.valueOf(food.size()));
        gamePanel.getScore().setText(String.valueOf(mainPlayer.getScore()));
    }


    boolean eatFood(Coord c) {
        if (gameField.get(c).getCellType() != CellType.FOOD)
            return false;
        else {
            gameField.get(c).setCellType(CellType.EMPTY); //по идее в этом нет смысла, ведь там все равно окажется голова змеи
            food.remove(c);
            //System.out.println(food.size());
            //System.out.println("Food eaten");
            return true;
        }
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public GameField getGameField() {
        return gameField;
    }

    public int getNumberOfAliveSnakes() {
        int counter = 0;
        for (Player p : players) {

            if (p.isAlive())
                ++counter;

        }
        return counter;
    }

    public Coord convertCoord(Coord c) {
        int x = c.getX();
        int y = c.getY();
        int newX = x;
        int newY = y;
        int height = gameField.getHeight();
        int width = gameField.getWidth();
        if (x < 0)
            newX = x + height;
        if (y < 0)
            newY = y + width;
        if (x > height - 1)
            newX = x - height;
        if (y > width - 1)
            newY = y - width;
        return new Coord(newX, newY);
    }

    public int getNewPlayerId() {
        int oldValue = newPlayerId;
        newPlayerId++;
        return oldValue;
    }

    public Player getMainPlayer() {
        return mainPlayer;
    }

    public void setMainPlayer(Player mainPlayer) {
        this.mainPlayer = mainPlayer;
    }

    public SnakesProto.GameMessage createStateMessage(int senderID, int receiverID) {

        int stateOrder = getNewStateOrder();
        //System.out.println("Message with state " + stateOrder + " created");
        ArrayList<Snake> snakes = getPlayersSnakes(players);
        //ДОБАВИТЬ ПРЕОБРАЗОВАНИЕ КООРДИНАТ У ЗМЕЕК

        return MessageCreator.createStateMsg(messageNumerator.getNewMessageNumber(), senderID, receiverID, stateOrder, players, snakes, food, gameConfig);
    }

    private ArrayList<Snake> getPlayersSnakes(ArrayList<Player> players) {
        ArrayList<Snake> snakes = new ArrayList<Snake>();
        int count = 0;
        for (Player p : players) {
            if (p.getRole() != SnakesProto.NodeRole.VIEWER) {
                snakes.add(p.getSnake());
                count++;
            }
        }
        //System.out.println(count + " snakes added to state msg");
        return snakes;
    }

    public void sendStateToPlayers() {
        ArrayList<Message> stateMessages = new ArrayList<>();
        synchronized (players) {
            for (Player p : players) {
                if (p.getId() != mainPlayer.getId()) { //всем кроме себя
                    try {
                        SnakesProto.GameMessage msg = createStateMessage(mainPlayer.getId(), p.getId());
                        //System.out.println("before sender");
                        stateMessages.add(new Message(msg, InetAddress.getByName(p.getIpAddress()), p.getPort()));
                        //sender.send(msg, InetAddress.getByName(p.getIpAddress()), p.getPort(), true);
                        //System.out.println("after sender");
                        System.out.println("Message " + msg.getMsgSeq() + " with state [" + msg.getState().getState().getStateOrder() + "] sended to " + p.getId());
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        for (Message m : stateMessages) {
            sender.send(m.getGameMessage(), m.getReceiverIP(), m.getReceiverPort(), true);
        }

    }

    public int getNewStateOrder() {
        int oldStateOrder = stateOrder;
        ++stateOrder;
        return oldStateOrder;
    }

    public int getStateOrder() {
        return stateOrder;
    }

    public void setGameState(SnakesProto.GameState state, SnakesProto.NodeRole role) {

        gameField = new GameField(gameConfig.getWidth(), gameField.getHeight());
        ArrayList<Coord> food = new ArrayList<Coord>();
        List<SnakesProto.GameState.Snake> snakesFromState = state.getSnakesList();

        for (SnakesProto.GameState.Coord c : state.getFoodsList()) {
            Coord coord = new Coord(c);
            food.add(coord); //непонятно надо ли это делать или достаточно изменить значения ячеек поля?
            gameField.getCells()[c.getX()][c.getY()].setCellType(CellType.FOOD);
        }
        this.food = food; //это потом отрисуется в drawGraphic
        System.out.println("NUMBER OF FOOD: " + state.getFoodsList().size());

        for (SnakesProto.GameState.Snake s : snakesFromState) {
/*
            ArrayList<Coord> newCoords = new ArrayList<>();
            for (SnakesProto.GameState.Coord c : s.getPointsList()) {
                newCoords.add(new Coord(c));
            }
            ArrayList<Coord> convertedCoords = pointsToCells(newCoords, gameField.getWidth(), gameField.getHeight());

            for (Coord c : convertedCoords) {

                gameField.getCells()[c.getX()][c.getY()].setCellType(CellType.SNAKE);
            }
*/
            for (SnakesProto.GameState.Coord c : s.getPointsList()) {
                gameField.getCells()[c.getX()][c.getY()].setCellType(CellType.SNAKE);
            }
        }

        if (role == DEPUTY) {
            gameConfig = new GameConfig(state.getConfig());
            ArrayList<Player> newPlayers = new ArrayList<>();
            for (SnakesProto.GamePlayer p : state.getPlayers().getPlayersList()) {
                Player newPlayer = new Player(p);
                SnakesProto.GameState.Snake s = getSnakeById(newPlayer.getId(), snakesFromState);
                if (s != null) {
                    newPlayer.setSnake(new Snake(s));
                    newPlayers.add(newPlayer);
                }
            }
            players = newPlayers;
        }
    }

    public ArrayList<Coord> cellsToPoints(ArrayList<Coord> cells) {
        /*
         * Перевод из всех точек змеи в координаты узлов  //КОГДА ЗМЕЙКА ПЕРЕСЕКАЕТ ГРАНИЦУ ВСЕ ЛОМАЕТСЯ
         * */
        ArrayList<Coord> points = new ArrayList<Coord>();
        Coord head = cells.get(cells.size() - 1);
        points.add(new Coord(head.getX(), head.getY()));

        int mainX = head.getX(), mainY = head.getY();
        int curX = head.getX(), curY = head.getY(), nextX = head.getX(), nextY = head.getY();

        for (int i = 0; i < points.size(); i++) {
            int j = points.size() - 1 - i;
            nextX = points.get(j).getX();
            nextY = points.get(j).getY();
            if ((nextX - mainX != 0) && (nextY - mainY != 0)) {
                points.add(new Coord(curX - mainX, curY - mainY));
                mainX = curX;
                mainY = curY;
            }
            curX = nextX;
            curY = nextY;
        }
        Coord tail = new Coord(nextX - mainX, nextY - mainY);
        points.add(tail);
        return points;
    }

    private static ArrayList<Coord> pointsToCells(List<Coord> offsets, int width, int height) {
        ArrayList<Coord> coords = new ArrayList<>();
        Coord offsetCoord;
        Coord prevCoord;

        if (!offsets.isEmpty()) {
            prevCoord = offsets.remove(0);

            coords.add(prevCoord);
            while (!offsets.isEmpty()) {
                offsetCoord = offsets.remove(0);
                int mod = abs(offsetCoord.getX()) + abs(offsetCoord.getY());

                if ((offsetCoord.getX() != 0 && offsetCoord.getY() != 0) || mod == 0) {
                    //System.out.println("illegal snake state");
                    break;
                }

                offsetCoord.setX(offsetCoord.getX() / mod);
                offsetCoord.setY(offsetCoord.getY() / mod);

                while (mod > 0) {
                    prevCoord.setX((prevCoord.getX() + offsetCoord.getX()) % width);
                    prevCoord.setY((prevCoord.getY() + offsetCoord.getY()) % height);

                    if (prevCoord.getX() < 0) {
                        prevCoord.setX(prevCoord.getX() + width);
                    }
                    if (prevCoord.getY() < 0) {
                        prevCoord.setY(prevCoord.getY() + height);
                    }

                    coords.add(prevCoord);
                    mod--;
                }
            }
            return coords;
        } else {
            return null;
        }
    }

    public GamePanel getGamePanel() {
        return gamePanel;
    }

    public void checkDeaths() {
        ArrayList<Player> deadPlayers = new ArrayList<Player>();
        for (Player p1 : players) {
            if (!(p1.getRole() == SnakesProto.NodeRole.VIEWER)) {
                Coord head = p1.getSnake().getHeadCoords();
                if (p1.getSnake().eatMyself()) {
                    System.out.println("OMG SELF EATING!!!!");
                    deadPlayers.add(p1);
                    //смерть
                }
                for (Player p2 : players) {
                    if ((p2.getSnake().getPoints().contains(head)) && (p1 != p2) && (p2.getRole() != SnakesProto.NodeRole.VIEWER)) {
                        System.out.println("OMG BUMPING TO OTHER SNAKE!!!!!!!!!!!!");
                        deadPlayers.add(p1);
                        p2.addPointsToScore(1);
                        //смерть
                    }
                }
            }
        }
        for (Player p : deadPlayers) {
            p.setRole(SnakesProto.NodeRole.VIEWER);
            SnakesProto.GameMessage roleChangeMsg = MessageCreator.createRoleChangeMsg(messageNumerator.getNewMessageNumber(), mainPlayer.getId(), p.getId(), SnakesProto.NodeRole.MASTER, SnakesProto.NodeRole.VIEWER);
            try {
                if (p != mainPlayer) { //себе не отправлть
                    sender.send(roleChangeMsg, InetAddress.getByName(p.getIpAddress()), p.getPort(), true);
                }
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }

            System.out.println("player is dead");
        }
    }

    public SnakesProto.GameMessage createAck(SnakesProto.GameMessage gameMessage) {
        SnakesProto.GameMessage ackMsg = SnakesProto.GameMessage.newBuilder()
                .setAck(SnakesProto.GameMessage.AckMsg.newBuilder().build())
                .setMsgSeq(gameMessage.getMsgSeq())
                .setSenderId(getMainPlayer().getId())
                .build();
        return ackMsg;
    }

    boolean isPlayer(String ip, int port) {
        for (Player p : players) {
            if ((p.getIpAddress().equals(ip)) && (p.getPort() == port))
                return true;
        }
        return false;
    }

    public MessageNumerator getMessageNumerator() {
        return messageNumerator;
    }

    public void checkPlayersActivity() {
        ArrayList<Player> exitPlayers = new ArrayList<>();
        ArrayList<Message> messagesToSend = new ArrayList<>();
        synchronized (players) {
            for (Player p : players) {
                if ((System.currentTimeMillis() - p.getLastActivity() > gameConfig.getNodeTimeoutMs()) && (p != mainPlayer)) {
                    System.out.println("............................................PLAYER " + p.getId() + " LEFT THE GAME");
                    exitPlayers.add(p);
                }
            }
            for (Player p : exitPlayers) {
                if (p.getRole() == DEPUTY) {
                    for (Player p2 : players) {
                        if ((p2 != mainPlayer) && (p2 != p) && (!exitPlayers.contains(p2))) {
                            System.out.println(p.getId() + "!=" + mainPlayer.getId());
                            p2.setRole(SnakesProto.NodeRole.DEPUTY);
                            SnakesProto.GameMessage roleChangeMsg = MessageCreator.createRoleChangeMsg(messageNumerator.getNewMessageNumber(), mainPlayer.getId(), p2.getId(), SnakesProto.NodeRole.MASTER, SnakesProto.NodeRole.DEPUTY);
                            System.out.println("Old deputy left. Choose new DEPUTY");

                            try {
                                messagesToSend.add(new Message(roleChangeMsg, InetAddress.getByName(p2.getIpAddress()), p2.getPort()));
                            } catch (UnknownHostException e) {
                                e.printStackTrace();
                            }
 /*                           try {
                                sender.send(roleChangeMsg, InetAddress.getByName(p2.getIpAddress()), p2.getPort(), true);
                            } catch (UnknownHostException e) {
                                e.printStackTrace();
                            }*/
                            break;
                        }
                    }
                }
                players.remove(p);
                System.out.println("Player removed due to exit");
            }
        }
        for (Message m : messagesToSend) { //АНТИ ДЕДЛОК
            sender.send(m.getGameMessage(), m.getReceiverIP(), m.getReceiverPort(), true);
        }
    }

    public SnakesProto.GameState.Snake getSnakeById(int id, List<SnakesProto.GameState.Snake> snakesFromState) {
        for (SnakesProto.GameState.Snake s : snakesFromState) {
            if (s.getPlayerId() == id) {
                return s;
            }
        }
        return null;
    }

    public Player getPlayerById(int id, ArrayList<Player> players) {
        for (Player p : players) {
            if (p.getId() == id) {
                return p;
            }
        }
        return null;
    }

    public void notifyAboutNewMaster() {
        ArrayList<Message> roleChangeMessages = new ArrayList<>();
        synchronized (players) {
            for (Player p : players) {
                if (p.getId() != mainPlayer.getId()) { //всем кроме себя
                    try {
                        SnakesProto.GameMessage msg = MessageCreator.createRoleChangeMsg(messageNumerator.getNewMessageNumber(), mainPlayer.getId(), p.getId(), MASTER, NORMAL);
                        roleChangeMessages.add(new Message(msg, InetAddress.getByName(p.getIpAddress()), p.getPort()));
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        for (Message m : roleChangeMessages) {
            sender.send(m.getGameMessage(), m.getReceiverIP(), m.getReceiverPort(), true);
        }
    }

    public void chooseNewDeputy() {
        for (Player p : players) {
            if (p != mainPlayer) {
                p.setRole(SnakesProto.NodeRole.DEPUTY);
                SnakesProto.GameMessage roleChangeMsg = MessageCreator.createRoleChangeMsg(messageNumerator.getNewMessageNumber(), mainPlayer.getId(), p.getId(), SnakesProto.NodeRole.MASTER, SnakesProto.NodeRole.DEPUTY);
                System.out.println("I WANT THIS PLAYER TO BE " + roleChangeMsg.getRoleChange().getReceiverRole().toString());
                try {
                    sender.send(roleChangeMsg, InetAddress.getByName(p.getIpAddress()), p.getPort(), true);
                } catch (UnknownHostException e) {
                    continue;
                }
                break;
            }
        }
    }
}
