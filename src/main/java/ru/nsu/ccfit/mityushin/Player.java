package ru.nsu.ccfit.mityushin;

import java.net.InetAddress;
import java.util.ArrayList;

public class Player {
    private Snake snake;
    private int score = 0;
    private String name;       // Имя игрока (для отображения в интерфейсе)
    private int id;          // Уникальный идентификатор игрока в пределах игры
    private String ip_address; // IPv4 или IPv6 адрес игрока в виде строки (отправитель не знает свой IP, поэтому указывает тут пустую строку)
    private int port;        // Порт UDP-сокета игрока
    private SnakesProto.NodeRole role;     // Роль узла в топологии
    private long lastActivity = System.currentTimeMillis();

    public Player(int startX, int startY, String name, SnakesProto.NodeRole role, int id, String ip, int port) { //УБРАТЬ ЭТОТ ГОВНО КОНСТРУКТОР
        this.name = name;
        this.role = role;
        this.id = id;
        ip_address = ip;
        this.port = port;
        snake = new Snake(startX, startY);
        this.name = name;
    }

    public Player(String name, SnakesProto.NodeRole role, int id, String ip_address, int port) {
        this.name = name;
        this.role = role;
        this.id = id;
        this.ip_address = ip_address;
        this.port = port;
        this.name = name;
        snake = new Snake();
    }

    public Player(SnakesProto.GamePlayer gamePlayer) {
        name = gamePlayer.getName();
        id = gamePlayer.getId();
        ip_address = gamePlayer.getIpAddress();
        port = gamePlayer.getPort();
        role = gamePlayer.getRole();
        score = gamePlayer.getScore();
    }

    public void setSnake(Snake snake) {
        this.snake = snake;
    }

    public Snake getSnake() {
        return snake;
    }

    public boolean isAlive() {
        if (snake == null)
            return false;
        if (snake.getSnakeState() == SnakesProto.GameState.Snake.SnakeState.ALIVE)
            return true;
        else return false;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void addPointsToScore(int score) {
        this.score += score;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getIpAddress() {
        return ip_address;
    }

    public int getPort() {
        return port;
    }

    public SnakesProto.NodeRole getRole() {
        return role;
    }

    public void setRole(SnakesProto.NodeRole role) {
        this.role = role;
    }

    public long getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(long lastActivity) {
        this.lastActivity = lastActivity;
    }

    public static Player getPlayerByIpAndPort(ArrayList<Player> players, InetAddress ip, int port) {
        for (Player p : players) {
            if ((p.getIpAddress() == ip.getHostAddress()) && (p.getPort() == port)) {
                return p;
            }
        }
        return null;
    }
}
