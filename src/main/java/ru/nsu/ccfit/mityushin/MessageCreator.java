package ru.nsu.ccfit.mityushin;

import com.google.protobuf.Message;
import org.w3c.dom.Node;

import java.util.ArrayList;

public class MessageCreator {

    public static SnakesProto.GameMessage createAnnouncementMsg(long msgSeq, ArrayList<Player> players, GameConfig gameConfig, boolean canJoin) {

        return SnakesProto.GameMessage.newBuilder()
                .setAnnouncement(SnakesProto.GameMessage.AnnouncementMsg.newBuilder()
                        .setPlayers(createPlayers(players))
                        .setConfig(createGameConfig(gameConfig))
                        .setCanJoin(canJoin).build())
                .setMsgSeq(msgSeq)
                .build();
    }

    public static SnakesProto.GameMessage createPingMsg(long msgSeq, int receiverID) {
        return SnakesProto.GameMessage.newBuilder()
                .setPing(SnakesProto.GameMessage.PingMsg.newBuilder().build())
                .setReceiverId(receiverID)
                .setMsgSeq(msgSeq)
                .build();
    }

    public static SnakesProto.GameMessage createPingMsg(long msgSeq) {
        return SnakesProto.GameMessage.newBuilder()
                .setPing(SnakesProto.GameMessage.PingMsg.newBuilder().build())
                .setMsgSeq(msgSeq)
                .build();
    }

    public static SnakesProto.GameMessage createAckMsg(long msgSeq, int senderID) {
        return SnakesProto.GameMessage.newBuilder()
                .setAck(SnakesProto.GameMessage.AckMsg.newBuilder().build())
                .setMsgSeq(msgSeq)
                .setSenderId(senderID)
                .build();
    }

    public static SnakesProto.GameMessage createAckMsg(long msgSeq, int senderID, int receiverID) {
        return SnakesProto.GameMessage.newBuilder()
                .setAck(SnakesProto.GameMessage.AckMsg.newBuilder().build())
                .setMsgSeq(msgSeq)
                .setSenderId(senderID)
                .setReceiverId(receiverID)
                .build();
    }

    public static SnakesProto.GameMessage createAckMsg(long msgSeq) {
        return SnakesProto.GameMessage.newBuilder()
                .setAck(SnakesProto.GameMessage.AckMsg.newBuilder().build())
                .setMsgSeq(msgSeq)
                .build();
    }

    public static SnakesProto.GameMessage createStateMsg(long msgSeq, int senderID, int receiverID, int stateOrder, ArrayList<Player> players, ArrayList<Snake> snakes, ArrayList<Coord> food, GameConfig config) {

        return SnakesProto.GameMessage.newBuilder()
                .setState(SnakesProto.GameMessage.StateMsg.newBuilder()
                        .setState(createState(stateOrder, players, snakes,food, config))
                        .build())
                .setMsgSeq(msgSeq)
                .setSenderId(senderID)
                .setReceiverId(receiverID)
                .build();
    }

    public static SnakesProto.GameMessage createJoinMsg(long msgSeq, String name) {
        return SnakesProto.GameMessage.newBuilder()
                .setJoin(SnakesProto.GameMessage.JoinMsg.newBuilder().setName(name).build())
                .setMsgSeq(msgSeq)
                .build();
    }

    public static SnakesProto.GameMessage createSteerMsg(long msgSeq, int senderID, int receiverID, SnakesProto.Direction direction) {
        return SnakesProto.GameMessage.newBuilder()
                .setSteer(SnakesProto.GameMessage.SteerMsg.newBuilder()
                        .setDirection(direction)
                        .build())
                .setMsgSeq(msgSeq)
                .setSenderId(senderID)
                .setReceiverId(receiverID)
                .build();
    }

    public static SnakesProto.GameMessage createSteerMsg(long msgSeq, int senderID, SnakesProto.Direction direction) {
        return SnakesProto.GameMessage.newBuilder()
                .setSteer(SnakesProto.GameMessage.SteerMsg.newBuilder()
                        .setDirection(direction)
                        .build())
                .setMsgSeq(msgSeq)
                .setSenderId(senderID)
                .build();
    }

    public static SnakesProto.GameMessage createRoleChangeMsg(long msgSeq, int senderID, int receiverID, SnakesProto.NodeRole senderRole, SnakesProto.NodeRole receiverRole){
        return SnakesProto.GameMessage.newBuilder()
                .setRoleChange(SnakesProto.GameMessage.RoleChangeMsg
                        .newBuilder()
                        .setSenderRole(senderRole)
                        .setReceiverRole(receiverRole))
                .setSenderId(senderID)
                .setReceiverId(receiverID)
                .setMsgSeq(msgSeq)
                .build();
    }

    public static SnakesProto.GameMessage createErrorMsg(long msgSeq, String errorMessage){
        return SnakesProto.GameMessage.newBuilder()
                .setError(SnakesProto
                        .GameMessage.ErrorMsg
                        .newBuilder()
                        .setErrorMessage(errorMessage)
                        .build())
                .setMsgSeq(msgSeq)
                .build();
    }

    public static SnakesProto.GameState createState(int stateOrder, ArrayList<Player> players, ArrayList<Snake> snakes, ArrayList<Coord> food, GameConfig config) {
        SnakesProto.GameState.Builder builder = SnakesProto.GameState.newBuilder()
                .setStateOrder(stateOrder)
                .setPlayers(createPlayers(players))
                .setConfig(createGameConfig(config));


        for (int i = 0; i < food.size(); i++) {
            builder.addFoods(createCoord(food.get(i)));
            //builder.setFoods(i, createCoord(food.get(i)));
        }


        for (int i = 0; i < players.size(); i++) {
            Player p = players.get(i);
            if (p.getRole() != SnakesProto.NodeRole.VIEWER) {
                builder.addSnakes(createSnake(p.getSnake(), p));
            }
        }

        return builder.build();
    }

    public static SnakesProto.GameState.Snake createSnake(Snake s, Player p) {
        SnakesProto.GameState.Snake.Builder builder = SnakesProto.GameState.Snake.newBuilder();

        builder.setState(s.getSnakeState())
                .setHeadDirection(s.getDirection())
                .setPlayerId(p.getId());

        for (int i = 0; i < s.getPoints().size(); i++) {
            builder.addPoints(createCoord(s.getPoints().get(i)));
            //builder.setPoints(i, createCoord(s.getPoints().get(i)));
        }
        return builder.build();
    }

    public static SnakesProto.GamePlayer createPlayer(Player player) {
        SnakesProto.GamePlayer.Builder newPlayer = SnakesProto.GamePlayer.newBuilder();
        newPlayer.setName(player.getName())
                .setId(player.getId())
                .setIpAddress(player.getIpAddress())
                .setPort(player.getPort())
                .setRole(player.getRole())
                .setScore(player.getScore());
        return newPlayer.build();
    }

    public static SnakesProto.GamePlayers createPlayers(ArrayList<Player> players) {
        SnakesProto.GamePlayers.Builder gamePlayers = SnakesProto.GamePlayers.newBuilder();
        for (Player p : players) {
            gamePlayers.addPlayers(createPlayer(p));
        }
        return gamePlayers.build();
    }

    public static SnakesProto.GameConfig createGameConfig(GameConfig gameConfig) {
        SnakesProto.GameConfig.Builder newConfig = SnakesProto.GameConfig.newBuilder();
        return newConfig
                .setWidth(gameConfig.getWidth())
                .setHeight(gameConfig.getHeight())
                .setFoodStatic(gameConfig.getFoodStatic())
                .setFoodPerPlayer(gameConfig.getFoodPerPlayer())
                .setStateDelayMs(gameConfig.getStateDelayMs())
                .setDeadFoodProb(gameConfig.getDeadFoodProb())
                .setPingDelayMs(gameConfig.getPingDelayMs())
                .setNodeTimeoutMs(gameConfig.getNodeTimeoutMs())
                .build();
    }

    public static SnakesProto.GameState.Coord createCoord(Coord c) {
        return SnakesProto.GameState.Coord.newBuilder().setX(c.getX()).setY(c.getY()).build();
    }

    public static SnakesProto.NodeRole createNodeRole(NodeRole role) {
        switch (role) {
            case MASTER:
                return SnakesProto.NodeRole.MASTER;
            case NORMAL:
                return SnakesProto.NodeRole.NORMAL;
            case DEPUTY:
                return SnakesProto.NodeRole.DEPUTY;
            case VIEWER:
                return SnakesProto.NodeRole.VIEWER;
        }
        return null;
    }
}
