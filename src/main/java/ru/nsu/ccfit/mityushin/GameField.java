package ru.nsu.ccfit.mityushin;

public class GameField {
    private int width;
    private int height;
    private Cell[][] cells;

    public GameField(int width, int height) {
        this.width = width;
        this.height = height;
        cells = new Cell[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                cells[i][j] = new Cell(i, j);
            }
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Cell[][] getCells() {
        return cells;
    }

    public Cell get(Coord coords){
        /*
        * Вернуть ячейку поля по ее координатам
        * */
        return cells[coords.getX()][coords.getY()];
    }

    public int getNumberOfEmptyCells() {
        int num = 0;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (cells[i][j].getCellType() == CellType.EMPTY)
                    ++num;
            }
        }
        return num;
    }

    public void draw() {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                switch (cells[i][j].getCellType()) {
                    case EMPTY:
                        System.out.print('-');
                        break;
                    case SNAKE:
                        System.out.print('•');
                        break;
                    case FOOD:
                        System.out.print('♥');
                        break;
                }
            }
            System.out.print('\n');
        }
    }

    public void placeFood() {

    }
}
