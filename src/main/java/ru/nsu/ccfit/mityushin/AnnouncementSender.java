package ru.nsu.ccfit.mityushin;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;

public class AnnouncementSender implements Runnable {

    private int multicastPort;
    private String multicastIP;
    private DatagramSocket socket;
    private GameConfig gameConfig;
    private ArrayList<Player> players;
    private MessageNumerator messageNumerator;

    public AnnouncementSender(int multicastPort, String multicastIP, DatagramSocket socket, GameConfig gameConfig, ArrayList<Player> players, MessageNumerator messageNumerator) {
        this.multicastPort = multicastPort;
        this.multicastIP = multicastIP;
        this.socket = socket;
        this.gameConfig = gameConfig;
        this.players = players;
        this.messageNumerator = messageNumerator;
    }

    public void run() {
        try {
            InetAddress group = InetAddress.getByName(multicastIP);
            long end, start = System.currentTimeMillis();

            while (true) {
                end = System.currentTimeMillis();
                if (end - start > 1000) {

                    SnakesProto.GameMessage msg = MessageCreator.createAnnouncementMsg(messageNumerator.getNewMessageNumber(), players, gameConfig, true);
                    socket.send(new DatagramPacket(msg.toByteArray(), msg.toByteArray().length, group, multicastPort));
                    System.out.println("Announcement " + msg.getMsgSeq() + " sended to multicast group");
                    start = end;
                }
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
