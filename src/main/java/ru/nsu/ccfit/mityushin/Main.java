package ru.nsu.ccfit.mityushin;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;

import static java.net.InetAddress.getByName;

public class Main {
    private static int port = 3372;
    private static String name = "Max";
    private static final int BUFSIZE = 1024;
    private static SnakesProto.NodeRole nodeRole = SnakesProto.NodeRole.NORMAL; //MASTER - создать игру, NORMAL - присоединиться к игре

    public static void main(String[] args) {

        try {
            DatagramSocket socket = new DatagramSocket(port);
            HashMap<HostInfo, AtomicLong> lastMessageTime = new HashMap<>();
            Sender sender = new Sender(socket, lastMessageTime);
            MessageNumerator messageNumerator = new MessageNumerator();

            switch (nodeRole) {
                case MASTER:
                    GameConfig gameConfig = new GameConfig(15, 20, 2, 1, 500, (float) 0.3, 2000, 2500);
                    Game newGame = new Game(gameConfig, socket, messageNumerator, sender, lastMessageTime);
                    Player firstPlayer = new Player(name, SnakesProto.NodeRole.MASTER, 0, "", port);
                    newGame.setMainPlayer(firstPlayer);
                    newGame.addNewPlayer(firstPlayer);
                    Thread receiver = new Thread(new Receiver(socket, newGame, messageNumerator, sender, lastMessageTime));
                    receiver.start();
                    newGame.getMainPlayer().setRole(SnakesProto.NodeRole.MASTER);
                    newGame.start(null, 0);
                    break;
                case NORMAL:
                    try {
                        MulticastSocket multicastSocket = new MulticastSocket(9192);
                        multicastSocket.joinGroup(getByName("239.192.0.4"));

                        DatagramPacket receivedPacket = new DatagramPacket(new byte[BUFSIZE], BUFSIZE);
                        multicastSocket.receive(receivedPacket);
                        byte[] rcvBuf = Arrays.copyOfRange(receivedPacket.getData(), 0, receivedPacket.getLength());
                        SnakesProto.GameMessage gameMessage = SnakesProto.GameMessage.parseFrom(rcvBuf);

                        switch (gameMessage.getTypeCase()) {
                            case ANNOUNCEMENT:
                                SnakesProto.GameMessage.AnnouncementMsg announcementMsg = gameMessage.getAnnouncement();
                                System.out.println(receivedPacket.getAddress() + ":" + receivedPacket.getPort() + " is available to join");
                                System.out.println("joining to " + receivedPacket.getAddress() + ":" + receivedPacket.getPort());
                                SnakesProto.GameMessage joinMsg = MessageCreator.createJoinMsg(messageNumerator.getNewMessageNumber(), name);
                                sender.send(joinMsg, receivedPacket.getAddress(), receivedPacket.getPort(), true);
                                Game game = new Game(new GameConfig(announcementMsg.getConfig()), socket, messageNumerator, sender, lastMessageTime, new Player(name, SnakesProto.NodeRole.NORMAL, 0, "", port));
                                game.getMainPlayer().setRole(SnakesProto.NodeRole.NORMAL);
                                game.start(receivedPacket.getAddress(), receivedPacket.getPort());

                                break;
                            case ERROR:
                                break;
                            case ACK:
                                break;
                        }
                    } catch (UnknownHostException e) {
                        System.out.println("UNKNOWN HOST EXCEPTION");
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }
}
