package ru.nsu.ccfit.mityushin;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class Resender implements Runnable {

    private HashMap<Long, Message> unconfirmedMessages;
    private HashMap<Long, Long> confirmationTable;

    private DatagramSocket socket;
    private Sender sender;
    private ArrayList<Player> players;
    private int pingDelay;
    private AtomicLong lastMessageTime;
    private static final int SEC_WAIT = 1;

    public Resender(DatagramSocket socket, Sender sender, ArrayList<Player> players, int pingDelay) {
        this.unconfirmedMessages = sender.getUnconfirmedMessages();
        this.confirmationTable = sender.getConfirmationTable();
        this.socket = socket;
        this.sender = sender;
        this.players = players;
        this.pingDelay = pingDelay;
    }

    @Override
    public void run() {
        while (true) {
            ArrayList<Message> messagesToResend = new ArrayList<>(); //Сообщения которые надо добавить в неподтвержденные
            ArrayList<Message> messagesToDelete = new ArrayList<>(); //Сообщения которые надо перестать переотправлять
            synchronized (unconfirmedMessages) {
               // System.out.println("lock (unconfirmedMessages) in resender");
                Iterator it = unconfirmedMessages.entrySet().iterator();

                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    long msg_seq = (long) pair.getKey();
                    Message message = (Message) pair.getValue();

                    synchronized (players) {
                        if (!playerInTheGame(message.getGameMessage().getReceiverId())) {
                            messagesToDelete.add(message);
                            continue;
                        }
                    }

                    Long time;
                    synchronized (confirmationTable) {
                        time = confirmationTable.get(msg_seq);
                    }
                    try {
                        if (System.currentTimeMillis() - time > pingDelay) {
                            messagesToResend.add(message);
                            System.out.println(msg_seq + " [" + message.getGameMessage().getTypeCase().toString() + "] is need to be resended");
                        }
                    } catch (NullPointerException e) {
                        System.out.println(".............................................................. was trying to find " + msg_seq + " in confirmation table");
                    }
                }
            }

            //System.out.println("unlock (unconfirmedMessages) in resender");
            deleteMessages(messagesToDelete);
            resendMessages(messagesToResend); //так сделано, чтобы не попадать в дедлок
        }
    }

    public int getPingDelay() {
        return pingDelay;
    }

    public void setPingDelay(int pingDelay) {
        this.pingDelay = pingDelay;
    }

    public void resendMessages(ArrayList<Message> messagesToResend) {
        if (!messagesToResend.isEmpty()) {
            for (Message m : messagesToResend) { //так сделано, чтобы не попадать в дедлок
                System.out.print("RESEND: ");
                sender.send(m.getGameMessage(), m.getReceiverIP(), m.getReceiverPort(), false);
            }
        }
    }

    public void deleteMessages(ArrayList<Message> messagesToDelete) {
        if (!messagesToDelete.isEmpty()) {
            for (Message m : messagesToDelete) {
                unconfirmedMessages.remove(m);
            }
        }
    }

    public boolean playerInTheGame(int id) {
        for (Player p : players) {
            if (p.getId() == id)
                return true;
        }
        return false;
    }
}
