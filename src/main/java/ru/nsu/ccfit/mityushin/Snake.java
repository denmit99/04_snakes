package ru.nsu.ccfit.mityushin;

import java.util.*;

public class Snake {
    private SnakesProto.GameState.Snake.SnakeState snakeState = SnakesProto.GameState.Snake.SnakeState.ALIVE; //
    private SnakesProto.Direction direction;
    private ArrayList<Coord> points = new ArrayList<Coord>(); //В начале списка хвост, в конце голова

    public Snake() {
        direction = SnakesProto.Direction.UP;
    }

    public Snake(int startX, int startY) {
        Coord tail, head = new Coord(startX, startY);
        Random random = new Random();
        switch (Math.abs(random.nextInt()) % 4) {
            case 0:
                direction = SnakesProto.Direction.UP;
                tail = new Coord(startX + 1, startY);
                break;
            case 1:
                direction = SnakesProto.Direction.DOWN;
                tail = new Coord(startX - 1, startY);
                break;
            case 2:
                direction = SnakesProto.Direction.LEFT;
                tail = new Coord(startX, startY + 1);
                break;
            case 3:
                direction = SnakesProto.Direction.RIGHT;
                tail = new Coord(startX, startY - 1);
                break;
            default:
                tail = new Coord(startX + 1, startY);
                break;
        }
        //Coord tail = new Coord(startX + 1, startY); //ЭТО НАДО ИЗМЕНИТЬ ЧТОБЫ ХВОСТ ГЕНЕРИЛСЯ РАНДОМНО ОТ ГОЛОВЫ и проверять что не встал на место еды
        //direction = SnakesProto.Direction.DOWN; //а это определить в зависимости расположения хвоста
        points.add(head);
        points.add(tail);
    }

    public Snake(SnakesProto.GameState.Snake snake) {
        snakeState = snake.getState();
        direction = snake.getHeadDirection();
        for (SnakesProto.GameState.Coord c : snake.getPointsList()) {
            points.add(new Coord(c));
        }
    }

    public void changeDirection(SnakesProto.Direction direction) {
        if (oppositeDirections(this.direction, direction)) //если не пытаемся повернуть вспять то поворачиваем
            return;
        else this.direction = direction;
    }

    public void printSnakeCoordinates() {
        ListIterator<Coord> it = points.listIterator();
        while (it.hasNext()) {
            Coord c = it.next();
            System.out.println("(" + c.getX() + ", " + c.getY() + ")");
        }

    }

    public void moveForward(Coord newHeadCoords) { //убрать хвост, сдвинуть голову,
        points.remove(0);
        points.add(newHeadCoords);
    }

    public Coord getNextCoord() {
        Coord headCoords = getHeadCoords();
        Coord newHeadCoords = headCoords;

        switch (direction) { //ЕСЛИ МЫ ДВИЖЕМСЯ ВЛЕВО ТО НЕ МОЖЕМ ПОВЕРНУТЬСЯ ВПРАВО ЭТО НАДО КОНТРОЛИРОВАТЬ В НАЖАТИИ НА КНОПКУ
            case UP:
                newHeadCoords = new Coord(headCoords.getX() - 1, headCoords.getY());
                break;
            case DOWN:
                newHeadCoords = new Coord(headCoords.getX() + 1, headCoords.getY());
                break;
            case LEFT:
                newHeadCoords = new Coord(headCoords.getX(), headCoords.getY() - 1);
                break;
            case RIGHT:
                newHeadCoords = new Coord(headCoords.getX(), headCoords.getY() + 1);
                break;
        }
        return newHeadCoords;
    }

    public void growUp(Coord c) {
        points.add(c);
    }

    public Coord getTailCoords() {
        return points.get(0);
    }

    public Coord getHeadCoords() {
        return points.get(points.size() - 1);
    }

    public SnakesProto.Direction getDirection() {
        return direction;
    }

    public SnakesProto.GameState.Snake.SnakeState getSnakeState() {
        return snakeState;
    }

    public ArrayList<Coord> getPoints() {
        return points;
    }

    public boolean eatMyself() {
        Coord head = getHeadCoords();
        for (int i = 0; i < points.size(); i++) {
            Coord c = points.get(i);
            if ((c != head) && (c.equals(head))) //если не головной узел змеи имеет такие же координаты что и голова
                return true;
        }
        return false;
    }

    public boolean oppositeDirections(SnakesProto.Direction d1, SnakesProto.Direction d2) {
        if (((d1 == SnakesProto.Direction.UP) && (d2 == SnakesProto.Direction.DOWN)) ||
                ((d1 == SnakesProto.Direction.DOWN) && (d2 == SnakesProto.Direction.UP)) ||
                ((d1 == SnakesProto.Direction.LEFT) && (d2 == SnakesProto.Direction.RIGHT)) ||
                ((d1 == SnakesProto.Direction.RIGHT) && (d2 == SnakesProto.Direction.LEFT)))
            return true;
        else return false;
    }

    public void setSnakeState(SnakesProto.GameState.Snake.SnakeState snakeState) {
        this.snakeState = snakeState;
    }

    public String toString() {
        String s = "";
        for (Coord c : points) {
            s += c.toString();
        }
        return s;
    }

}
