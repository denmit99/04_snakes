package ru.nsu.ccfit.mityushin;

public class GameConfig {
    private int width;           // Ширина поля в клетках (от 10 до 100)
    private int height;          // Высота поля в клетках (от 10 до 100)
    private int foodStatic;      // Количество клеток с едой, независимо от числа игроков (от 0 до 100)
    private float foodPerPlayer;  // Количество клеток с едой, на каждого игрока (вещественный коэффициент от 0 до 100)
    private int stateDelayMs; // Задержка между ходами (сменой состояний) в игре, в миллисекундах (от 1 до 10000)
    private float deadFoodProb; // Вероятность превращения мёртвой клетки в еду (от 0 до 1).
    private int pingDelayMs;   // Задержка между отправкой ping-сообщений, в миллисекундах (от 1 до 10000)
    private int nodeTimeoutMs; // Таймаут, после которого считаем что узел-сосед отпал, в миллисекундах (от 1 до 10000)

    public GameConfig(int width, int height, int foodStatic, float foodPerPlayer, int stateDelayMs, float deadFoodProb, int pingDelayMs, int nodeTimeoutMs) {
        this.width = width;
        this.height = height;
        this.foodStatic = foodStatic;
        this.foodPerPlayer = foodPerPlayer;
        this.stateDelayMs = stateDelayMs;
        this.deadFoodProb = deadFoodProb;
        this.pingDelayMs = pingDelayMs;
        this.nodeTimeoutMs = nodeTimeoutMs;
    }

    public GameConfig(SnakesProto.GameConfig gameConfig){
        width = gameConfig.getWidth();
        height = gameConfig.getHeight();
        foodStatic = gameConfig.getFoodStatic();
        foodPerPlayer = gameConfig.getFoodPerPlayer();
        stateDelayMs = gameConfig.getStateDelayMs();
        deadFoodProb = gameConfig.getDeadFoodProb();
        pingDelayMs = gameConfig.getPingDelayMs();
        nodeTimeoutMs = gameConfig.getNodeTimeoutMs();
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getStateDelayMs() {
        return stateDelayMs;
    }

    public float getFoodPerPlayer() {
        return foodPerPlayer;
    }

    public int getFoodStatic() {
        return foodStatic;
    }

    public float getDeadFoodProb() {
        return deadFoodProb;
    }

    public int getPingDelayMs() {
        return pingDelayMs;
    }

    public int getNodeTimeoutMs() {
        return nodeTimeoutMs;
    }
}
